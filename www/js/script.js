var timersIntervals = {
    intervals: []
};
var elements = document.getElementsByClassName("timer").length;
window.onload = function () {
    elements = document.getElementsByClassName("timer").length;
}
function add() {
    let taskName = document.getElementById("addTaskTextId").value;
    let taskId = ++elements;
    let container = document.getElementById("container");
    container.innerHTML += "<div class=\"timer\" id=\"element" + taskId + "\"><span class=\"tastText\">" + taskName + "</span><div class=\"timerElem\"><span class=\"hours\">00</span>:<span class=\"minutes\">00\</span>:<span class=\"seconds\">00</span>:<span class=\"tens\">00</span></div><br/><button class=\"button-start\" onclick=\"start(this)\">Start</button><button class=\"button-stop\" onclick=\"stop(this)\">Stop</button><button class=\"button-log\" onclick=\"log(this)\">Log time</button><button class=\"button-del\" onclick=\"document.getElementById('element" + taskId + "').remove()\">Delete task</button><br/>.......</div>";
}

function start(buttonElement) {
    let timerContainer = buttonElement.parentElement;
    let appendTens = timerContainer.getElementsByClassName("tens")[0];
    let appendSeconds = timerContainer.getElementsByClassName("seconds")[0];
    let appendMinutes = timerContainer.getElementsByClassName("minutes")[0];
    let appendHours = timerContainer.getElementsByClassName("hours")[0];
    appendTens.innerHTML = "00";
    appendSeconds.innerHTML = "00";
    appendMinutes.innerHTML = "00";
    appendHours.innerHTML = "00";

    let time = {
        hours: 0,
        minutes: 0,
        seconds: 0,
        tens: 0
    };

    let Interval;
    let firstStart = true;
    let timer = null;
    for (let i = 0; i < timersIntervals.intervals.length; i++) {
        if (timersIntervals.intervals[i].name === timerContainer.id) {
            timer = timersIntervals.intervals[i];
            firstStart = false;
            break;
        }
    }
    if(timer == null) timer = {name: timerContainer.id, interval: Interval, started: false};

    //let Interval = timer.interval;
    if(!timer.started) {
        clearInterval(timer.interval);
        timer.interval = setInterval(startTimer, 10);
        timer.started = true;
    }
    if(firstStart) {
        timersIntervals.intervals.push(timer);
    } else {
        for (let i = 0; i < timersIntervals.intervals.length; i++) {
            if (timersIntervals.intervals[i].name === timerContainer.id) {
                timersIntervals.intervals[i] = timer;
                break;
            }
        }
    }

    function startTimer() {
        time.tens++;
        if (time.tens > 99) {
            time.seconds++;
            time.tens = 0;
        }
        if (time.seconds > 59) {
            time.minutes++;
            time.seconds = 0;
        }
        if (time.minutes > 59) {
            time.hours++;
            time.minutes = 0;
        }

        if (time.tens <= 9) {
            appendTens.innerHTML = "0" + time.tens;
        } else {
            appendTens.innerHTML = "" + time.tens;
        }

        if (time.seconds <= 9) {
            appendSeconds.innerHTML = "0" + time.seconds;
        } else {
            appendSeconds.innerHTML = "" + time.seconds;
        }

        if (time.minutes <= 9) {
            appendMinutes.innerHTML = "0" + time.minutes;
        } else {
            appendMinutes.innerHTML = "" + time.minutes;
        }

        if (time.hours <= 9) {
            appendHours.innerHTML = "0" + time.hours;
        } else {
            appendHours.innerHTML = "" + time.hours;
        }
    }
}

function stop(buttonElement) {
    let timerContainer = buttonElement.parentElement;
    timer = null;
    for (let i = 0; i < timersIntervals.intervals.length; i++) {
        if (timersIntervals.intervals[i].name === timerContainer.id) {
            timer = timersIntervals.intervals[i];
            break;
        }
    }
    if(timer == null || !timer.started) return;
    clearInterval(timer.interval);
    timer.started = false;
    for (let i = 0; i < timersIntervals.intervals.length; i++) {
        if (timersIntervals.intervals[i].name === timerContainer.id) {
            timersIntervals.intervals[i] = timer;
            break;
        }
    }
}